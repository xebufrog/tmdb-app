# TMDB App

## State model

```
state: {
  loading: false,
  genres: [],
  nowPlaying: []

```

## Input

* Minimum rating input with a range between 0 and 10, increments of 0.5 and a default set to 3. This can be implemented with radio buttons or numeric stepper

* Multiple genres input (checkboxes). Must only contain genres from the TMDb API that are in the returned movie result set.


## Output 

* Display a list of movies, each showing their title, genres and poster image.

* The movies should be ordered by popularity (most popular first - popularity property).

* Movies should be filterable by multiple genres, the user should have the ability to toggle movies depending on all of its assigned genres. For example if 'Action' and 'Drama' genres are selected listed movies must have both 'Action' and 'Drama' genres.

* Movies should also be filterable by their rating (`vote_average property`). i.e If rating was set to 5, you would expect to see all movies with a rating of 5 or higher.

* The input APIs should only be called once. **Need** to describe how this was handled in detail. Emphaisis on `Promise.all()`

## Todo


* try catch around each get request and rethrow
* try catch around promises.all
* 


* need to improve the tests to show how I can mock test promise.all
* mock for 404
* mock for 401
* mock for 500

* better styling