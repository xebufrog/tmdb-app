import React, { Component } from 'react'
import Checkbox from '../components/Checkbox';

class GenreFilter extends Component {

  createCheckbox = (item) => (
    <Checkbox
      name={item.name}
      onChange={this.props.onChange}
      key={item.id}
    />
  )

  createCheckboxes = () => (
    this.props.genres.map(this.createCheckbox)
  )

  render() {
   
    return (
      <div className="genre-filter">
        { this.createCheckboxes() }
      </div>
    )
  }
}

export default GenreFilter