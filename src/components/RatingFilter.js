import React, { Component } from 'react'
import Radio from '../components/Radio';

class RatingFilter extends Component {
 
  createRadioButton = (name, value) => (
    <Radio
      name={name}
      onChange={this.props.onChange}
      key={value}
      value={value}
    />
  )

  createRadioButtons = () => {
    let buttons = []
    for (let i = 1; i <= 10; i += .5) {
      buttons.push(this.createRadioButton('Rating', i))
    }
    return buttons
  }

  render() {

    return (
      <div className="rating">
        <p>Rating</p>
        {this.createRadioButtons()}
      </div>
    )
  }
}

export default RatingFilter