import React from 'react'


export const displayGenres = ((ids, genres) => {
  let g = []
  ids.forEach(id => {
    g.push(genres[id])
  });

  return g
})

export default (props) => {
  
  let { title, vote_average, poster_path, genre_ids } = props.show
  let genreList = displayGenres(genre_ids, props.genres).sort().join(', ')
  return (<div>
    <span>This is a {title}</span>
    <p>{genreList}</p>
    <p>{vote_average}</p>
    <img src={`https://image.tmdb.org/t/p/w200/${poster_path}`} alt={title} />
  </div>)
}
