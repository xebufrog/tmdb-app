import React from 'react'
import Show from './Show'

export default (props) => {
  
  let { genres, shows } = props

  return (
    <div>
      {shows.map(obj => 
        <Show key={obj.id} show={obj} genres={genres}/>
      )}
    </div>
  )
}
