
import React from 'react'

export default ({ type = 'radio', name, value, onChange }) => (
  <label>
    <input type={type} value={value} name={name} onChange={onChange} />
    {value}
  </label>
)