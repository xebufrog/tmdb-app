
import React from 'react'

export default ({ type = 'checkbox', name, checked, onChange }) => (
  <label>
    <input type={type} name={name} checked={checked} onChange={onChange} />
    {name}
  </label>
)