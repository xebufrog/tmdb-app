import React from 'react';
import App from './App';
import { filterByGenre, filterByRating } from './lib/helpers'

//mock data for testing purposes
import NowPlaying from './__mocks__/__data__/nowplaying'

it('should display a header', () => {
  const wrapper = shallow(<App />)

  expect(wrapper.find('h1.App-title').length).toBe(1)

})

it('filter shows on rating', () => {
  let shows = NowPlaying.results
  let results = filterByRating(7, shows)
  expect(results.length).toBeLessThan(20)
})

it('filter shows on genre', () => {
  let genres = [18, 28]
  let shows = NowPlaying.results

  let results = filterByGenre(genres, shows)
  expect(results.length).toBeLessThan(shows.length)
  
})