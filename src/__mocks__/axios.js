/**
 * * To avoid making actual HTTP requests am mocking axios using
 * * Jest's mock functionality
 * * Am delbrately returning an empty JSON object in order to reuse the test
 */
export default {
  get: jest.fn(() => Promise.resolve( { data: {} } ))
}