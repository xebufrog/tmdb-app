import axios from 'axios'
/** *
 * * I like to declare endpoints as constants. Typically I work with APIs that
 * * potentially are different depending on environment.
 * 
 * */

const NOW_PLAYING_ENDPOINT = 'https://api.themoviedb.org/3/movie/now_playing'
const GENRES_ENDPOINT = 'https://api.themoviedb.org/3/genre/movie/list'
const IMAGE_ENDPOINT = 'https://image.tmdb.org/t/p/w500/'
const API_KEY = process.env.REACT_APP_TMDB_APP_KEY
const DEFAULT_PARAMS = {
  params: {
    api_key: process.env.REACT_APP_TMDB_APP_KEY,
    language: 'en-GB'
  }
}

/** 
 * ? The input API endpoints should only be called once.
 */

export const fetchNowPlaying = async (page = 1) => {

  const params = { ...DEFAULT_PARAMS.params, region: 'GB', page: page }
  const response = await axios
    .get(NOW_PLAYING_ENDPOINT, { params: params })
  // check response code
  return response.data
}

/**
 * Fetch all now playing shows in the UK.
 * tmdb returns pafge 1 of x by default.
 * Determin the total number of pages and 
 * concurrently retrieve them
 */

export const fetchAllNowPlaying = () => {

  return new Promise(async (resolve, reject) => {

    try {

      // *Get first page of results
      let results0 = await fetchNowPlaying()
      let { results, total_pages } = results0

      // *Now we know the total no. of pages request
      // *remaining results concurrently
      let promises = []

      for (let p = 2; p <= total_pages; p++) {
        promises.push(fetchNowPlaying(p))
      }

      let nowPlaying = results
      Promise
        .all(promises)
        .then((r) => {
          r.forEach(item => {
            nowPlaying = nowPlaying.concat(item.results)

          })

          resolve(nowPlaying)
        })
    }
    catch (error) {
      reject(error)
    }
  })

}

export const fetchGenres = async () => {

  const response = await axios
    .get(GENRES_ENDPOINT, DEFAULT_PARAMS)

  return response.data.genres
}

export const fetchAll = async () => {

  let promises = [fetchAllNowPlaying(), fetchGenres()]
  let [nowPlaying, genres] = await Promise.all(promises)
  console.log('nowPlaying: ', nowPlaying)
  console.log('genres: ', genres)

  return { nowPlaying, genres }

}

