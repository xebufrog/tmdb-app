import { fetchNowPlaying, fetchGenres } from '../tmdb'
import mockAxios from '../../__mocks__/axios'

const mockGenre = [{ 'id': 28, 'name': 'Action' }]
const mockNowPlaying = [{
  "vote_count": 1308,
  "id": 353081,
  "video": false,
  "vote_average": 7.3,
  "title": "Mission: Impossible - Fallout",
  "popularity": 141.012,
  "poster_path": "/AkJQpZp9WoNdj7pLYSj1L0RcMMN.jpg",
  "original_language": "en",
  "original_title": "Mission: Impossible - Fallout",
  "genre_ids": [
    12,
    28,
    53
  ],
  "backdrop_path": "/5qxePyMYDisLe8rJiBYX8HKEyv2.jpg",
  "adult": false,
  "overview": "When an IMF mission ends badly, the world is faced with dire consequences. As Ethan Hunt takes it upon himself to fulfil his original briefing, the CIA begin to question his loyalty and his motives. The IMF team find themselves in a race against time, hunted by assassins while trying to prevent a global catastrophe.",
  "release_date": "2018-07-25"
}]

// TODO: move into either .env or constants file 
const NOW_PLAYING_ENDPOINT = 'https://api.themoviedb.org/3/movie/now_playing'
const GENRES_ENDPOINT = 'https://api.themoviedb.org/3/genre/movie/list'
const IMAGE_ENDPOINT = 'https://image.tmdb.org/t/p/w500/'

const DEFAULT_PARAMS = {
  params: {
    api_key: process.env.REACT_APP_TMDB_APP_KEY,
    language: 'en-GB'
  }
}

// first test genres
it('calls axios and returns genres from tmdb', async () => {
  
  mockAxios.get.mockImplementationOnce(() => Promise.resolve({
    data: {
      genres: mockGenre
    }
  }))

  const results = await fetchGenres()
  expect(results).toEqual(mockGenre)
  //expect(mockAxios.get).toHaveBeenCalledTimes(1)
  expect(mockAxios.get).toHaveBeenCalledWith(GENRES_ENDPOINT, DEFAULT_PARAMS)
})

//TODO: ensure

// //then test now playing
// it('calls axios and returns now playing results', async () => {

//   mockAxios.get.mockImplementationOnce( () => Promise.resolve({
//     data: {
//       results: mockNowPlaying
//     }
//   }))

//   const results = await fetchNowPlaying()
//   const params = { ...DEFAULT_PARAMS.params, region: 'GB' }

//   expect(results).toEqual(mockNowPlaying)
//   expect(mockAxios.get).toHaveBeenCalledTimes(2) 
//   expect(mockAxios.get).toHaveBeenCalledWith(NOW_PLAYING_ENDPOINT, { params: params })
// })

// do we need to test the image