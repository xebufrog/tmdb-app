import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { fetchNowPlaying, fetchAll, fetchAllNowPlaying } from './services/tmdb'
import { filterByRating, filterByGenre, getGenreByName } from './lib/helpers'
import ShowList from './components/ShowList'
import GenreFilter from './components/GenreFilter'
import RatingFilter from './components/RatingFilter'

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      nowPlaying: [],
      genres: [],
      genreFilter: [],
      rating: 3,
      displayShows: []
    }
  }
  
  init() {
    
  }

  async componentDidMount() {

    try {

      let results = await fetchAll()

      let { nowPlaying, genres } = results

      let genreIndex = {}
      
      genres.map(g => {
        return genreIndex[g.id] = g.name
      })

      genres = genres.sort((a, b, key='name') => {
        var strA = a[key].toUpperCase()
        var strB = b[key].toUpperCase()

        if (strA < strB) {
          return -1;
        }
        if (strA > strB) {
          return 1;
        }

        // names must be equal
        return 0;
      })

      this.setState( { 
        loading: false,
        nowPlaying, 
        genres,
        genreIndex,
        displayShows: nowPlaying
      })

    } catch (error) {
      
    }
  }

  filterGenre(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;

    let filterBy
    let genreId = getGenreByName(item, this.state.genres)[0].id

    if (this.state.genreFilter.indexOf(genreId) > -1) {
      filterBy = this.state.genreFilter.filter(g => g !== genreId)
    } else {
      filterBy = [...this.state.genreFilter, genreId]
    }

    this.setState({ 
      genreFilter: filterBy, 
      displayShows: filterByGenre(filterBy, this.state.nowPlaying)
    })

  }

  filterRating(e) {
    const rating = e.target.value;

    this.setState({
      displayShows: filterByRating(rating, this.state.nowPlaying)
    })

  }

  render() {
   
    if (this.state.loading === true) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">TMDB Movie App</h1>
        </header>
        <h2 className="App-intro">
          A basic React application that displays the now playing movies/shows in the UK
        </h2>
        <RatingFilter onChange={this.filterRating.bind(this)} rating={this.state.rating} />
        <GenreFilter genres={this.state.genres} onChange={this.filterGenre.bind(this)}/>
        <ShowList shows={this.state.displayShows} genres={this.state.genreIndex}  />
      </div>
    );
  }
}

export default App
