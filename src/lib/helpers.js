export const filterByRating = (rating = 0, shows) => {
    return shows.filter(obj =>
      obj.vote_average >= rating
    )
}

export const filterByGenre = (genres=[], shows=[]) => {
  
  let filtered = []
  
  shows.map(obj => {
    if (genres.every(id => obj.genre_ids.includes(id))) {
      filtered.push(obj)
    }
  })
 
  return filtered
}

export const getGenreByName = (name, genres) => {
  return genres.filter(genre => genre.name === name)
}